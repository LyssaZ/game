
import java.util.Random;

public class Enemy {



	//Enemy types, 0 dps, 1 healer, 2 tank, 3 boss, 4 special
	
	private static final String typenames[] = {"Knight", "healer", "tank", "boss", "special"};
	public static final int AmountOfClasses = typenames.length;
    private int floor;
	private int type;
	private int currenthealth;
	private String name;
	private int status = 0;
	private int statusCounter = 0;
	private boolean charge;
	private int damagBonus = 1;
//Names
	
	private final static String PreNames[] = {"Mono","Band", "Experiment ", "R", "Queen ", "Prince ","Chairman"};
	private final static String LastNames[] = {"Kuma","etto", "b02ef", "q", "fiction", "hyi", "Emoticon","Drek"};
	
	
	
	//0 health,1 damage, 2 armor, 3 magic power

	private static final int DPSbaseStats[] = {10,5,3,0};	
	private static final int healerbaseStats[] = {7,2,5,10};
	private static final int tankbaseStats[] = {15,4,7,2};
	private static final int bossbaseStats[] = {40,8,8,0};
	private static final int specialbaseStats[] = {5,5,5,5};
	
	
	//0 void, 1 basic attack, 2 Charge, 3 charge attack, 4 Protect, 5 heal, 6   Super charge attack, 7 Special attack, 8 special block, 9 Special magic 
	

	private static final int DPSAttacks[] = {1,2,3};
	private static final int healerAttacks[] = {1,4,5};
	private static final int tankAttacks[] = {1,2,4,6};
	private static final int bossAttacks[] = {4,1,2,6};
	private static final int specialAttacks[] = {7,8,9};
	
	
	//Creates the classes
	private static final int DPS[][] = {DPSbaseStats,DPSAttacks};
	private static final int healer[][] = {healerbaseStats,healerAttacks};
	private static final int tank[][] = {tankbaseStats,tankAttacks};
	private static final int boss[][] = {bossbaseStats,bossAttacks};
	private static final int special[][] = {specialbaseStats, specialAttacks};
	private static final int classes[][][] = {DPS,healer,tank,boss, special};
	
	
	
	
	public Enemy(int floor, int type) {
		
		
		if(0 <= type && type < classes.length) {
			Random rand = new Random();
			
			this.type = type;
			this.floor = floor;
		
			this.currenthealth = classes[this.type][0][0] * (10 + (floor/2));
			this.name = PreNames[rand.nextInt(PreNames.length)] + LastNames[rand.nextInt(LastNames.length)]; 
			
		}
		
		
		
		
		
		
		
	}
	
	public int getAttackPower() {
		
		return classes[this.type][0][1] * (1+ (floor/2));
	
	}

	public int getArmor() {
		
		return classes[this.type][0][2] * (1+ (floor/2));
		
	}
	
	public int getMagic() {
		return classes[this.type][0][3] * (1+ (floor/2));
		
	}
	

	public int getCurrenthealth() {
		return currenthealth;
	}

	public void setCurrenthealth(int currenthealth) {
		this.currenthealth = currenthealth;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		
		return typenames[type];
		
	}
	
	public void Check() {
		System.out.println("You are facing " + this.name+"!\n" + 
				this.name + " is a " + this.getType()+ " with the following stats\nAttack: "+ this.getAttackPower() + 
				"\nArmor: " + this.getArmor() + "\nHealth: " + this.getCurrenthealth() + "\nMagic: " + this.getMagic()
				
						);
		
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getStatusCounter() {
		return statusCounter;
	}

	public void setStatusCounter(int statusCounter) {
		this.statusCounter = statusCounter;
	}

	public boolean getCharge() {
		return charge;
	}

	public void setCharge(boolean charge) {
		this.charge = charge;
	}

	public int getDamagBonus() {
		return damagBonus;
	}

	public void setDamagBonus(int damagBonus) {
		this.damagBonus = damagBonus;
	}
public int getTypeId(){
	return type;
	
}
	
	
}





