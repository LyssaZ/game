
import java.util.Random;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



public class CombatSystem {
	
	private static void status(Player p, Enemy e) {
		
	
		
		
		if(p.getStatus() == 0 && e.getStatus() == 0) {
			return;
			
		}
		if(e.getStatus() == 1) {
		
				e.setCurrenthealth(e.getCurrenthealth()- 10);
				if(e.getStatusCounter() == 0) {
					e.setStatusCounter(5);
						
					}else {
						e.setStatusCounter(e.getStatusCounter() -1);
						
					}
			} else if(e.getStatus() == 2) {
				if(!e.getCharge()) {
					e.setCharge(true);
					e.setDamagBonus(2);
					
				}else {
					e.setStatus(0);
					e.setCharge(false);
					e.setDamagBonus(0);
				}
				
				
		
			}else if(e.getStatus() == 3) {
				if(e.getStatusCounter() == 0) {
					e.setStatusCounter(5);
					
				}else {
					e.setStatusCounter(p.getStatusCounter()-1);
					
				}
				
				
			}else if(e.getStatus() == 4) {
				
			e.setCurrenthealth(e.getCurrenthealth() + (2 * e.getMagic() ));
			e.setStatusCounter(0);
				
			}
	
		if(p.getStatus() == 1) {
			
			p.setHealth(p.getHealth()- 10);
			if(p.getStatusCounter() == 0) {
				p.setStatusCounter(5);
				
			}else {
				p.setStatusCounter(p.getStatusCounter() -1);
				
			} 
			
			
			
		}else if(p.getStatus() == 2) {
			if(!p.getCharge()) {
				p.setCharge(true);
				p.setDamageBonus(2);
				
			}else {
				p.setStatus(0);
				p.setCharge(false);
				p.setDamageBonus(0);
			}
			
			
	
		}else if(p.getStatus() == 3) {
			if(p.getStatusCounter() == 0) {
				p.setStatusCounter(5);
				
			}else {
				p.setStatusCounter(p.getStatusCounter()-1);
				
				
			}
			
			
		}else if(p.getStatus() == 4) {
			
		p.setHealth(p.getHealth() + (2 * p.getmagicPower()) );
		p.setStatusCounter(0);
			
		}
		
			
		if(p.getStatusCounter() < 1) {
			p.setStatus(0);
			
			
		}
		if(e.getStatusCounter() < 1) {
			e.setStatus(0);
			
		}
		

	
		
		
	}
	
	public static void EnemyAttack(Player p, Enemy e){
		
		
		int ran = rand.nextInt(10);
		
	
		if(0.8 > p.getmaxHealth()/p.getHealth() && e.getTypeId() == BossID && !e.getCharge() || ran < 2 && e.getTypeId() == BossID && !e.getCharge()) {
			p.setCharge(true);
			p.setDamageBonus(2);
			System.out.println(e.getName()+ " used " + AttackNames[2]+"!\nBe aware");
			return;
			
			
		}else if(e.getTypeId() == BossID && e.getCharge()) {
			System.out.println(e.getName()+ " used " + AttackNames[2]+"!");
			p.setDamageBonus(1);
			
		} else if(true) {
			
			
			
		}
		
		
		
	
	}
	
	
	public static void DoBattle(int AttackID, Player p, Enemy e) {

		int damage = (int) (p.getAttackPower() *AttackStats[ p.getAttacks()[AttackID] ][0]* e.getDamagBonus()  - (e.getArmor()*0.8));
		
		
		if(AttackStats[p.getAttacks()[AttackID]][0] <= 0) {
			
			p.setStatus(AttackStats[AttackID][2]);
			return;
		}else {
			
			if( 0 <= e.getCurrenthealth()-damage) {
				
				e.setCurrenthealth(e.getCurrenthealth()-damage);
			
				if(e.getCurrenthealth() > 0) {
					System.out.println("The enemy lost " + damage +"hp, the enemy now has " + e.getCurrenthealth() +" hp left." );
				}
			}
			
			else {
			e.setCurrenthealth(e.getCurrenthealth()- 1);
			System.out.println("The enemy lost 1 hp, the enemy now has " + e.getCurrenthealth() +" hp left." );
			}
			
			
			
		}
		
		
		
		
	}
	
	
	
	private static final int BossID = 3;
	private static Random rand = new Random();
	private static BufferedReader input = new BufferedReader (new InputStreamReader(System.in));

	
public static final String AttackNames[] = {"Burn", "attack", "Charge", "Charge attack", "Protect","heal", "Super charge attack","Special attack", "special block", "Special magic"};	
public static final String AttackDescs[] = {""};
public static final String EffectNames[] = {};

//Special effects, 0 no Special effect, 1 burn, 2 charge needed to use charge attack, 3 protect , 4 heal
//Damage, skips the next turn, Special effect id, mp cost, cool down
//                                            void        attack         charge     charge attack    Protect      Heal
public static final int AttackStats[][] = {{5,0,1,20,2}, {4,0,0,10,0}, {0,1,2,5,0}, {10,0,0,0,0},   {0,0,3,10,5}, {0,0,4,20,3}, {10,1,2,0,0} };






	
	public static void Battle(Player p) throws IOException {

	
		int MonsterType;
		boolean battle = true;
		String Command;
		if(p.getFloor() < 10 | p.getFloor() < 20 ) {
			while(true) {
			MonsterType = rand.nextInt(Enemy.AmountOfClasses -2);
			if(MonsterType != BossID) {
				break;
			}
			}
			
		}else if(p.getFloor() == 10 | p.getFloor() == 20){
			MonsterType = BossID;
			
		} else {
			MonsterType = 6;
			
		}
		
		
		//Combat code 
		Enemy en = new Enemy(p.getFloor(), MonsterType);
		
		en.Check();
	
		while(battle) {
			System.out.println("What will you do?\nFight,Items,Check");
			Command = input.readLine().toLowerCase();
			
			EnemyAttack(p, en);
			
			if(Command.equals("check")) {
				
				en.Check();
				
			}
			else if(Command.equals("fight")) {
				System.out.println("You can use the following spells and attacks");
				p.getAttackNames();
				System.out.println("What will you do?");
				Command = input.readLine().toLowerCase();
				
				for(int i = 0; i < p.getAttacks().length; i++) {
				if(Command.equals(AttackNames[p.getAttacks()[i]].toLowerCase())) {
					
					System.out.println("You used " + AttackNames[p.getAttacks()[i]]);
					DoBattle(i,p,en);
					
					break;
					
					
				}
				
				
		
				
				
				}
				
				}
			
			if(p.getHealth() < 1|| en.getCurrenthealth() < 1)	{
				battle = false;
				
			}
				
				
			status(p,en);	
			p.genMp();
			
			
			}
			
	if(p.getHealth() > 0) {
		p.levelUp();
	}
			
	}
	
		
		
		
		
	} 
	
	
	
	

