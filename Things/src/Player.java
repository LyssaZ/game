
public class Player {

	private String name;
	private int level = 0;
	private int xp = 0;
	private int attackPower = 5;
	private int defPower = 5;
	private int magicPower = 5;
	private int gold = 0;
	private int health;
	private int floor = 0;
	private int mp;
	private int maxMp;
	private int status = 0;
	private int attacks[] = {1,5,0};
	private int mpGenAmount = 5;
	private int levelUpCap = 100;
	private int statusCounter = 0;
	private boolean charge = false;
	private int damageBonus = 1;
	private int maxHealth;
	
	
	
	
	public Player(String Name, int Level) {
		
	this.name = Name;	
	this.level = Level;
	this.mp = 100;
	this.maxMp = 100;
	this.health = 180;
	this.maxHealth = 180;
	
	
		
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getLevel() {
		return level;
	}


	public void setLevel(int level) {
		this.level = level;
	}


	public int getAttackPower() {
		return attackPower;
	}


	public void setAttackPower(int attackPower) {
		this.attackPower = attackPower;
	}


	public int getdefPower() {
		return defPower;
	}
	public void setdefPower(int defPower) {
		this.defPower = defPower;
	}


	public int getHealth() {
		return health;
	}


	public void setHealth(int health) {
		if(health >= this.maxHealth ) {
			this.health = maxHealth;
		}else {
			
		this.health = health;
	}
		
	}

	public int getGold() {
		return gold;
	}
	public void setGold(int gold) {
		this.gold = gold;
	}


	public int getFloor() {
		return floor;
	}


	public void setFloor(int floor) {
		this.floor = floor;
	}


	public int getMp() {
		return mp;
	}


	public void setMp(int mp) {
		this.mp = mp;
	}


	public int getMaxMp() {
		return maxMp;
	}


	public void setMaxMp(int maxMp) {
		this.maxMp = maxMp;
	}


	public int[] getAttacks() {
		return attacks;
	}


	public void setAttacks(int attacks[]) {
		this.attacks = attacks;
	}

	public void getAttackNames() {
		for(int i = 0; i < attacks.length; i++) {
			System.out.println(CombatSystem.AttackNames[attacks[i]]);
			
			
		}
		
		
		
	}
	
	public void check() {
		System.out.println("Name: " + this.name+"\n" + 
				"You have the following stats\nAttack: "+ this.attackPower + 
				"\nArmor: " + this.defPower + "\nHealth: " + this.health + "\nMagic: " + this.magicPower +"\nMagic points: " + this.mp+" max MP: " + this.maxMp);
				
		}
	
	
	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}
	
public int getmagicPower() {
	
	return this.magicPower;
	
}
public void setmagicPower(int magicPower) {
	this.magicPower = magicPower;
	
}

public void incMp(int amount){
	
	if(this.mp + amount > this.maxMp) {
		this.mp = this.maxMp;
		
	}else {
		this.mp+= amount;
		
	}
	
	
}

public void genMp(){
	
	if(this.mp + this.mpGenAmount > this.maxMp) {
		this.mp = this.maxMp;
		
	}else {
		this.mp+= this.mpGenAmount;
		
	}
	
	
}


public int getMpGenAmount() {
	return mpGenAmount;
}


public void setMpGenAmount(int mpGenAmount) {
	this.mpGenAmount = mpGenAmount;
}


public void levelUp() {
	
	if(this.level * this.levelUpCap >= this.xp) {
		this.level+=1;
		int skillPoints = 5;
		
		System.out.println("You reached level " + this.level + "!\nSelect the skills you want to increase");
		while(skillPoints > 0 ) {
			
			
			
			
		}
		
		
	}
	
	
	
}


public int getStatusCounter() {
	return statusCounter;
}


public void setStatusCounter(int statusCounter) {
	this.statusCounter = statusCounter;
}


public boolean getCharge() {
	return charge;
}


public void setCharge(boolean charge) {
	this.charge = charge;
}


public int getDamageBonus() {
	return damageBonus;
}


public void setDamageBonus(int damageBonus) {
	this.damageBonus = damageBonus;
}


public int getmaxHealth() {
	return this.maxHealth;
	
	
}
public void setmaxHealth(int maxHealth) {
	this.maxHealth = maxHealth;
	
}




	
	
}
